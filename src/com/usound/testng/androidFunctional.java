package com.usound.testng;

//Device					Device Name				SO	
//Moto G1 (rooteado 1/3)		TA9300A6QN				5.0.2				
//Samsung S6				06157df6090a6417		7.0
//Moto G4 (rafa)			ZY22446KQ7				6.0.1

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class androidFunctional {
	
	private AndroidDriver<AndroidElement> driver;
	String apk_name = "uSound_v2.5.1-freqLabel.apk";
	String device_name = "TA9300A6QN";
	String platform_version = "5.0.2";
	String nombre = "This is a test";
	String email = "prueba@usound.co";
	String licence = "Tu licencia está activa";
	String edad;
	String genero;
	String l125;
	String l250;
	String l500;
	String l1000;
	String l2000;
	String l4000;
	String l8000;
	String r125;
	String r250;
	String r500;
	String r1000;
	String r2000;
	String r4000;
	String r8000;
	
	// SECUENCIA INICIAL
	@Test(priority = 1)
	public void secInicial() throws Exception {
		Thread.sleep(2000);
		driver.resetApp();
		Thread.sleep(2000);
		driver.findElementById("com.newbrick.usound:id/acb_fwc_next").click(); // BtnBienvenido
		driver.findElementById("com.newbrick.usound:id/acb_fad_start").click(); // BtnComenzar
		Thread.sleep(3000);
		if (platform_version != "5.0.2") {
			driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click(); // Permiso
			Thread.sleep(3000);
			if (device_name == "06157df6090a6417") {
				System.out.println("sos un s6");
				TouchAction tap = new TouchAction(driver);
				tap.tap(991, 2336).perform().release();
			}else {
				driver.findElementById("com.newbrick.usound:id/acb_fad_start").click();// BtnComenzar
			}
		}
		// VERIFICAR OIDO
		Thread.sleep(3000);
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_left_channel_preview").click(); // Izquierdo
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		// TEST
		int i = 0;
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int testX = x / 2;
		int testY = y / 2;
		while (i <= 14) {
			TouchAction t = new TouchAction(driver);
			t.tap(testX, testY).perform().release();
			//Thread.sleep(500);
			i++;
		}
		// REGISTRO
		Thread.sleep(750);
		driver.findElementById("com.newbrick.usound:id/actv_ffr_name").click();// Name
		driver.findElementById("com.newbrick.usound:id/acactv_din_name").sendKeys(nombre);// Write
		driver.findElementById("com.newbrick.usound:id/acb_din_save").click();// Save
		driver.findElementById("com.newbrick.usound:id/actv_ffr_email").click();// Email
		driver.findElementById("com.newbrick.usound:id/acactv_die_email").sendKeys(email);// Write
		driver.findElementById("com.newbrick.usound:id/acb_die_save").click();// Save
		driver.findElementById("com.newbrick.usound:id/actv_ffr_age").click();// Edad
		driver.findElementById("com.newbrick.usound:id/acb_dap_save").click();// Save
		driver.findElementById("com.newbrick.usound:id/actv_ffr_gender").click();// Gender
		driver.findElementById("com.newbrick.usound:id/acrb_dig_female").click();// Female
		driver.findElementById("com.newbrick.usound:id/acrb_dig_male").click();// Male
		driver.findElementById("com.newbrick.usound:id/acb_dig_save").click();// Save
		edad = driver.findElementById("com.newbrick.usound:id/actv_ffr_age").getText();
		genero = driver.findElementById("com.newbrick.usound:id/actv_ffr_gender").getText();
		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/acb_ffr_start").click();// Start
		Thread.sleep(1000);
	}

	// NAVEGACIÓN ENTRE PESTAÑAS
	@Test(priority = 2 , groups = {"menu"})
	public void navegacionPrincipal() throws Exception {
		Thread.sleep(2000);
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest1X = x / 4;
		int pest1Y = y / 5;
		int pest2X = x / 2;
		int pest2Y = y / 5;
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest2X, pest2Y).perform().release(); // pestaña2
		Thread.sleep(2000);
		t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3
		Thread.sleep(2000);
		t = new TouchAction(driver);
		t.tap(pest1X, pest1Y).perform().release(); // pestaña 1
		Thread.sleep(2000);
		t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3
		Thread.sleep(2000);
		t = new TouchAction(driver);
		t.tap(pest2X, pest2Y).perform().release(); // pestaña2
		Thread.sleep(2000);
		t = new TouchAction(driver);
		t.tap(pest1X, pest1Y).perform().release(); // pestaña 1
		Thread.sleep(2000);
	}

	// TU NIVEL AUDITIVO
	@Test (priority = 3)
	public void navegacion_tuNivelAuditivo() throws Exception {
		Thread.sleep(2000);
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3
		Thread.sleep(3000);

		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// DetalleI
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		driver.findElementById("com.newbrick.usound:id/cfb_bsa_cancel").click();// CancelarAudiometria
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		driver.findElementById("com.newbrick.usound:id/cfb_bsa_save").click();// GuardarAudiometria
		driver.findElementById("com.newbrick.usound:id/acib_fer_back").click();// Volver 
		driver.findElementById("com.newbrick.usound:id/acb_fbr_redetails").click();// DetalleD
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		driver.findElementById("com.newbrick.usound:id/cfb_bsa_cancel").click();// CancelarAudiometria
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		driver.findElementById("com.newbrick.usound:id/cfb_bsa_save").click();// GuardarAudiometria
		driver.findElementById("com.newbrick.usound:id/acib_fer_back").click();// Volver
	}

	// MENU
	@Test(priority = 4, groups = {"menu"})
	public void navegaMenu() throws Exception {
		// ACCESO A MENU
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		
		
		driver.findElementByXPath("(//android.widget.RelativeLayout)[1]").click();// Opción1
		driver.findElementByClassName("android.widget.ImageButton").click();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
		driver.findElementByXPath("(//android.widget.RelativeLayout)[2]").click();// Opción2
		driver.findElementByClassName("android.widget.ImageButton").click();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click();// Opción3
		driver.findElementByClassName("android.widget.ImageButton").click();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click();// Opción4
		driver.findElementByClassName("android.widget.ImageButton").click();
		Thread.sleep(1000);
	}

	// ASISTENTE AUDITIVO
	@Test(priority = 5)
	public void asistenteAuditivo() throws Exception {
		Thread.sleep(2000);
		driver.findElementById("com.newbrick.usound:id/tb_fua_uaway_on_off").click();
		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/rl_fua_noisy").click();
		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/rl_fua_conversation").click();
		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/rl_fua_home").click();
		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/tb_fua_uaway_on_off").click();
		Thread.sleep(1000);
	}

	// VOLÚMEN
	@Test(priority = 6)
	public void mover_volumen() throws Exception {
		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/tb_fua_uaway_on_off").click();
		
		//VOLUMEN DERECHO
		AndroidElement seekBarDer = driver.findElementById("com.newbrick.usound:id/vsb_right_on");		
		int startY = seekBarDer.getLocation().getY();
		int endY = seekBarDer.getSize().getHeight();
		int xAxis = seekBarDer.getLocation().getX();
		int moveToYDirectionAt;
		int finBarraY = (int) (startY + endY - ((startY + endY) * 0.02));

		TouchAction act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.1);
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		Thread.sleep(1000);

		TouchAction act2 = new TouchAction(driver);
		moveToYDirectionAt = (int) (endY * 1.6);
		act2.longPress(xAxis, startY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		Thread.sleep(2000) ;
		
		//VOLUMEN IZQ
		AndroidElement seekBarIzq = driver.findElementById("com.newbrick.usound:id/vsb_left_on");		
		startY = seekBarIzq.getLocation().getY();
		endY = seekBarIzq.getSize().getHeight();
		xAxis = seekBarIzq.getLocation().getX();		
		finBarraY = (int) (startY + endY - ((startY + endY) * 0.02));

		act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.1);
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		Thread.sleep(1000);

		act2 = new TouchAction(driver);
		moveToYDirectionAt = (int) (endY * 1.6);
		act2.longPress(xAxis, startY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		Thread.sleep(2000);
	}
	
	// ECUALIZADOR
	@Test(priority = 7)
	public void mover_eq() throws Exception {
		Thread.sleep(500);
		driver.findElementById("com.newbrick.usound:id/tb_fua_uaway_on_off").click();
		Thread.sleep(2000);
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest2X = x / 2;
		int pest2Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest2X, pest2Y).perform().release(); // pestaña2
		Thread.sleep(2000);
		
				
		//EQ GRAVES
		AndroidElement seekBarGraves = driver.findElementById("com.newbrick.usound:id/vsb_fas_low_eq_on");		
		int startY = seekBarGraves.getLocation().getY();
		int endY = seekBarGraves.getSize().getHeight();
		int xAxis = seekBarGraves.getLocation().getX();
		int moveToYDirectionAt;
		int finBarraY = (int) (startY + endY - ((startY + endY) * 0.02));

		TouchAction act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.01);//Subir
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();

		TouchAction act2 = new TouchAction(driver);
		moveToYDirectionAt = (int) (endY * 2.5);//Bajar
		act2.longPress(xAxis, startY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		
		act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.01);//Subir
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		
		//EQ MEDIOS
		AndroidElement seekBarMedios = driver.findElementById("com.newbrick.usound:id/vsb_fas_mid_eq_on");		
		startY = seekBarMedios.getLocation().getY();
		endY = seekBarMedios.getSize().getHeight();
		xAxis = seekBarMedios.getLocation().getX();		
		finBarraY = (int) (startY + endY - ((startY + endY) * 0.02));

		act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.01);//Subir
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();

		act2 = new TouchAction(driver);
		moveToYDirectionAt = (int) (endY * 2.5);//Bajar
		act2.longPress(xAxis, startY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		
		act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.01);//Subir
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		
		//EQ AGUDOS
		AndroidElement seekBarAgudos = driver.findElementById("com.newbrick.usound:id/vsb_fas_high_eq_on");		
		startY = seekBarAgudos.getLocation().getY();
		endY = seekBarAgudos.getSize().getHeight();
		xAxis = seekBarAgudos.getLocation().getX();		
		finBarraY = (int) (startY + endY - ((startY + endY) * 0.02));

		act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.01);//Subir
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();

		act2 = new TouchAction(driver);
		moveToYDirectionAt = (int) (endY * 2.5);//Bajar
		act2.longPress(xAxis, startY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		
		act1 = new TouchAction(driver);
		moveToYDirectionAt = (int) (startY * 0.01);//Subir
		act1.longPress(xAxis, finBarraY).moveTo(xAxis, moveToYDirectionAt).release().perform();
		
		//PREDETERMINADO
		driver.findElementById("com.newbrick.usound:id/acb_fas_optimized_values").click();
		Thread.sleep(3000);		
	}

@Test(priority = 8)
	public void test_normal() throws Exception {
		Thread.sleep(2000);
		String l500;
		String l1000;
		String l2000;
		String l4000;
		String r500;
		String r1000;
		String r2000;
		String r4000;
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3
		int testX = x / 2;
		int testY = y / 2;
		int wait = 0;
		driver.findElementById("com.newbrick.usound:id/acb_fbr_new_test").click();// NuevoTest
		driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
		// VERIFICAR OIDO
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		int i = 0;
		while (i <= 14) {
			Thread.sleep(wait);
			t = new TouchAction(driver);
			t.tap(testX, testY).perform().release();
			i++;
		}
		String txt_Lnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_leresults").getText();
		String txt_Rnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_reresults").getText();
		String txt_Lpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_lepercent").getText();
		String txt_Rpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_repercent").getText();

		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// DetalleI
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();

		int val_l500 = Integer.valueOf(l500);
		int val_l1000 = Integer.valueOf(l1000);
		int val_l2000 = Integer.valueOf(l2000);
		int val_l4000 = Integer.valueOf(l4000);
		int val_r500 = Integer.valueOf(r500);
		int val_r1000 = Integer.valueOf(r1000);
		int val_r2000 = Integer.valueOf(r2000);
		int val_r4000 = Integer.valueOf(r4000);

		double lpercent = ((val_l500 + val_l1000 + val_l2000 + val_l4000) / 4 - 10) * 100 / 80;
		int trunc_lpercent = (int) Math.floor(lpercent);
		String lporc = String.valueOf(trunc_lpercent) + "%";

		double rpercent = ((val_r500 + val_r1000 + val_r2000 + val_r4000) / 4 - 10) * 100 / 80;
		int trunc_rpercent = (int) Math.floor(rpercent);
		String rporc = String.valueOf(trunc_rpercent) + "%";

		AssertJUnit.assertEquals(txt_Lnormal, "Sin pérdida\nde audición");
		AssertJUnit.assertEquals(txt_Rnormal, "Sin pérdida\nde audición");
		AssertJUnit.assertEquals(txt_Lpercent, lporc);
		AssertJUnit.assertEquals(txt_Rpercent, rporc);
	}

//MENU
	@Test(priority = 9, groups = {"menu"})
	public void menuDesplegable() throws Exception {
		// ACCESO A MENU
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();

		driver.findElementByXPath("(//android.widget.RelativeLayout)[2]").click();// Opción1
		driver.findElementByClassName("android.widget.ImageButton").click();		
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click();// Acercade
		driver.findElementById("com.newbrick.usound:id/actv_fa_tyc").click();// TyC
		Thread.sleep(1000);	
	}

  @BeforeMethod
  public void beforeMethod() throws MalformedURLException {
		File f = new File("src");
		File fs = new File(f, apk_name);
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, platform_version);
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, device_name);
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		cap.setCapability("noReset", "true");// No reset app
		driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
  }

	
  
  @AfterMethod
  public void afterMethod() {
		driver.quit();
  }

}
