package com.usound.testng;

//Device					Device Name				SO	
//Moto G1 (rooteado 1/3)	TA9300A6QN				5.0.2				
//Samsung S6				06157df6090a6417		7.0
//Moto G4 (rafa)			ZY22446KQ7				6.0.1

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JOptionPane;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class menuDesplegable {
	
	private AndroidDriver<AndroidElement> driver;
	String apk_name = "uSound_v2.5.1-freqLabel.apk";
	String device_name = "TA9300A6QN";
	String platform_version = "5.0.2";
	String nombre = "This is a test";
	String email = "prueba@usound.co";
	String licence = "Tu licencia está activa";
	String edad;
	String genero;	

//MENU
	
	//Verifica que funcione el link que redirige al usuario hacia los Terminos y Condiciones
	@Test (priority = 1)	
	public void AcercaDe_LinkTyC () throws Exception {
	
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
	
//		t = new TouchAction(driver);
//		t.tap(menuX, menuY).perform().release();
//		Thread.sleep(1000);
//		
//		driver.findElementByXPath("(//android.widget.RelativeLayout)[2]").click(); //Enviar comentarios
//		driver.findElementByClassName("android.widget.ImageButton").click();
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click();// Acerca de
		driver.findElementById("com.newbrick.usound:id/actv_fa_tyc").click();// TyC
		Thread.sleep(4000);
		
		//Verifica que se haya redirigido al usuario a la pagina de TyC
		String dirTyc = driver.findElementById("com.android.chrome:id/url_bar").getText();
		AssertJUnit.assertEquals(dirTyc, "https://www.usound.co/es/tyc/");
		
	} //Fin metodo AcercaDe_LinkTyC
	
	
	//Verifica que funcione el link que redirige al usuario hacia el Twitter de uSound
	@Test (priority = 2)
	public void AcercaDe_LinkTw () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click();// Acerca de
		driver.findElementById("com.newbrick.usound:id/aciv_fa_twitter_link").click();// Twitter
		Thread.sleep(4000);
		
		//Verifica que se haya redirigido al usuario a la pagina de Twitter de uSound
		String dirTw = driver.findElementById("com.android.chrome:id/url_bar").getText();
		AssertJUnit.assertEquals(dirTw, "https://mobile.twitter.com/uSound_Arg");
		
	} //Fin metodo AcercaDe_LinkTw
	
	//Verifica que funcione el link que redirige al usuario hacia el Facebook de uSound
	@Test (priority = 3)
	public void AcercaDe_LinkFb () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click(); //Acerca de
		driver.findElementById("com.newbrick.usound:id/aciv_fa_facebook_link").click(); //Facebook
		Thread.sleep(4000);
		
		//Verifica que se haya redirigido al usuario a la pagina de Facebook de uSound
		String dirFb = driver.findElementById("com.android.chrome:id/url_bar").getText();
		AssertJUnit.assertEquals(dirFb, "https://m.facebook.com/usoundNewbrick");
		
	} //Fin metodo AcercaDe_LinkFb
	
	//Verifica que funcione el link que redirige al usuario hacia el Sitio web de uSound
	@Test (priority = 4)
	public void AcercaDe_LinkWeb () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click(); //Acerca de
		driver.findElementById("com.newbrick.usound:id/aciv_fa_usound_link").click(); //Sitio web
		Thread.sleep(4000);
		
		//Verifica que se haya redirigido al usuario hacia el Sitio web de uSound
		String dirWeb = driver.findElementById("com.android.chrome:id/url_bar").getText();
		AssertJUnit.assertEquals(dirWeb, "https://www.usound.co/es/");
		
	} //Fin metodo AcercaDe_LinkFb
	
	//Verifica que funcione el link de enviar email a uSound
	@Test (priority = 5)
	public void AcercaDe_Contacto () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click(); //Acerca de
		driver.findElementById("com.newbrick.usound:id/actv_fa_email").click(); //Contacto
		Thread.sleep(4000);
		
		//Verifica que se haya redirigido hacia la redaccion de un nuevo correo
		String dirEmail = driver.findElementById("com.google.android.gm:id/to").getText();
		AssertJUnit.assertEquals(dirEmail, "<info@usound.co>, ");
		
	} //Fin metodo AcercaDe_Contacto
	
	//Verifica que funcione el link de la libreria MPAndroidChart
	@Test (priority = 6)
	public void AcercaDe_LibMPAC () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click(); //Acerca de
		driver.findElementById("com.newbrick.usound:id/actv_fa_mpAndroidChart_link").click(); //MPAC
		Thread.sleep(5000);
		
		//Verifica que se haya redirigido hacia la pagina web de la libreria
		String dirMPAC = driver.findElementById("com.android.chrome:id/url_bar").getText();
		AssertJUnit.assertEquals(dirMPAC, "https://github.com/PhilJay/MPAndroidChart");
		
	} //Fin metodo AcercaDe_LibMPAC
	
	//Verifica que funcione el link de la libreria Universal Image Loader
	@Test (priority = 7)
	public void AcercaDe_LibUIL () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click(); //Acerca de
		driver.findElementById("com.newbrick.usound:id/actv_fa_universalImageLoader_link").click(); //UIL
		Thread.sleep(5000);
		
		//Verifica que se haya redirigido hacia la pagina web de la libreria
		String dirUIL = driver.findElementById("com.android.chrome:id/url_bar").getText();
		AssertJUnit.assertEquals(dirUIL, "https://github.com/nostra13/Android-Universal-Image-Loader");
		
	} //Fin metodo AcercaDe_LibUIL
	
	//Verifica que funcione correctamente el boton Volver
	@Test (priority = 8)
	public void AcercaDe_BtnVolver () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click(); //Acerca de
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
				
	} //Fin metodo AcercaDe_BtnVolver
	
	//Verifica que funcione el boton desplegable de Ayuda - Test Auditivo
	@Test (priority = 9)
	public void ayuda_TestAuditivo () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Ayuda
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click(); //Ayuda
		driver.findElementById("com.newbrick.usound:id/rl_fh_title_help1").click(); //Test Auditivo		
		Thread.sleep(3000);
				
	} //Fin metodo ayuda_TestAuditivo
	
	//Verifica que funcione el boton desplegable de Ayuda - Capacidad Auditiva
	@Test (priority = 10)
	public void ayuda_CapAuditiva () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Ayuda
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click(); //Ayuda
		driver.findElementById("com.newbrick.usound:id/rl_fh_title_help2").click(); //Capacidad Auditiva		
		Thread.sleep(3000);
				
	} //Fin metodo ayuda_CapAuditiva
	
	//Verifica que funcione el boton desplegable de Ayuda - Asistente Auditivo
	@Test (priority = 11)
	public void ayuda_AsistAuditivo () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Ayuda
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click(); //Ayuda
		driver.findElementById("com.newbrick.usound:id/rl_fh_title_help3").click(); //Asistente auditivo		
		Thread.sleep(3000);
				
	} //Fin metodo ayuda_AsistAuditivo
	
	//Verifica que funcione el boton desplegable de Ayuda - Audiometria Medica
	@Test (priority = 12)
	public void ayuda_Audiometria () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Ayuda
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click(); //Ayuda
		driver.findElementById("com.newbrick.usound:id/rl_fh_title_help4").click(); //Audiometria	
		Thread.sleep(3000);
				
	} //Fin metodo ayuda_Audiometria
	
	//Verifica que funcione el link de FAQ de uSound
	@Test (priority = 13)
	public void ayuda_linkFAQ () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Ayuda
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click(); //Ayuda
		driver.findElementById("com.newbrick.usound:id/actv_fh_footer_2").click(); //FAQ
		Thread.sleep(5000);
		
		//Verifica que se haya redirigido hacia la pagina web de FAQ
		String dirFAQ = driver.findElementById("com.android.chrome:id/url_bar").getText();
		AssertJUnit.assertEquals(dirFAQ, "https://www.usound.co/es/help/");
				
	} //Fin metodo ayuda_linkFAQ
	
	//Verifica que funcione correctamente el boton Volver
	@Test (priority = 14)
	public void ayuda_BtnVolver () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Ayuda
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click(); //Ayuda
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
				
	} //Fin metodo ayuda_BtnVolver
	
	//Abre cada uno de los desplegables del menu
	@Test (priority = 15)
	public void navegaMenu () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Mi Cuenta
		driver.findElementByXPath("(//android.widget.RelativeLayout)[1]").click(); //Mi Cuenta
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Enviar comentarios
		driver.findElementByXPath("(//android.widget.RelativeLayout)[2]").click(); //Enviar comentarios
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Acerca de
		driver.findElementByXPath("(//android.widget.RelativeLayout)[3]").click(); //Acerca de
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Ayuda
		driver.findElementByXPath("(//android.widget.RelativeLayout)[4]").click(); //Ayuda
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en uSound Smart Earphones
		driver.findElementByXPath("(//android.widget.RelativeLayout)[5]").click(); //Ayuda
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
						
	} //Fin metodo navegaMenu
	
	//Verifica que funcione correctamente el boton Volver
	@Test (priority = 16)
	public void envMsj_BtnVolver () throws Exception {
		
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		//Hace click en Enviar comentarios
		driver.findElementByXPath("(//android.widget.RelativeLayout)[2]").click(); //Env. comentarios
		driver.findElementByClassName("android.widget.ImageButton").click(); //Boton Volver
		Thread.sleep(3000);
				
	} //Fin metodo envMsj_BtnVolver
	
	//Verifica que se envie correctamente un mensaje al correo de uSound
	@Test (priority = 17)
	public void envMsj_Enviar () throws Exception {
		
		//Inicializo variable de mensaje
		String msj = "Mensaje de prueba";
	
		//Abre el menu desplegable
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(2000);
		
		//Habilita el menu desplegable
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		Thread.sleep(1000);
	
		driver.findElementByXPath("(//android.widget.RelativeLayout)[2]").click(); //Env. comentarios
		driver.findElementById("com.newbrick.usound:id/et_ffb_input_message").sendKeys(msj); //Escribe msj
		Thread.sleep(2000);
		
		driver.findElementById("com.newbrick.usound:id/acb_ffb_send").click(); //Click en Enviar msj
		Thread.sleep(3000);
				
	} //Fin metodo envMsj_Enviar
	

	@BeforeMethod
  	public void beforeMethod() throws MalformedURLException {
		File f = new File("src");
		File fs = new File(f, apk_name);
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, platform_version);
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, device_name);
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		cap.setCapability("noReset", "true"); //No reset app
		driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
	}
  
	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}