package com.usound.testng;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;

//Device					Device Name				SO	
//Moto G1 (rooteado)		TA9300A6QN				5.0.2				
//Samsung S6				06157df6090a6417		7.0
//Moto G4 (rafa)			ZY22446KQ7				6.0.1

public class retrocompatibility {

	private AndroidDriver<AndroidElement> driver;
	String apk_name = "uSound_2.4.3r.apk";
	String device_name = "ZY22446KQ7";
	String platform_version = "6.0.1";
	String nombre = "Rafael Rodriguez";
	String email = "rafael@usound.co";
	String licence = "Tu licencia está activa";
	String edad;
	String genero;

	String l125;
	String l250;
	String l500;
	String l1000;
	String l2000;
	String l4000;
	String l8000;
	String r125;
	String r250;
	String r500;
	String r1000;
	String r2000;
	String r4000;
	String r8000;



	@BeforeMethod
	public void beforeMethod() throws MalformedURLException {
		File f = new File("src");
		File fs = new File(f, apk_name);
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, platform_version);
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, device_name);
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		cap.setCapability("noReset", "true");// No reset app
		driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
	}

		// Navega por la versión anterior
	@Test(priority = 1)
	public void navegaAnterior() throws Exception {			
		Thread.sleep(2000);
		driver.resetApp();
		Thread.sleep(4000);
		driver.findElementById("com.newbrick.usound:id/acb_fwc_next").click(); // BtnBienvenido
		driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
		Thread.sleep(3000);
		if (platform_version != "5.0.2") {
			driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click(); // Permiso
			driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click();// BtnComenzar 
		}
		// VERIFICAR OIDO
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_left_channel_preview").click(); // Izquierdo
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		
		// TEST
		int i = 0;
		Dimension dimensions = driver.manage().window().getSize();			
		int x = dimensions.getWidth();			
		int y = dimensions.getHeight();
		int testX = x/2;
		int testY = y/2;
		//System.out.println(String.valueOf(dimensions));
		while (i <= 14) {
			TouchAction t = new TouchAction(driver);
			t.tap(testX, testY).perform().release(); 			
			Thread.sleep(250);
			i++;
		}
		
		// REGISTRO
		Thread.sleep(750);
		driver.findElementById("com.newbrick.usound:id/actv_ffr_name").click();// Name
		driver.findElementById("com.newbrick.usound:id/acactv_din_name").sendKeys(nombre);// Write
		driver.findElementById("com.newbrick.usound:id/acb_din_save").click();// Save
		driver.findElementById("com.newbrick.usound:id/actv_ffr_email").click();// Email
		driver.findElementById("com.newbrick.usound:id/acactv_die_email").sendKeys(email);// Write
		driver.findElementById("com.newbrick.usound:id/acb_die_save").click();// Save
		driver.findElementById("com.newbrick.usound:id/actv_ffr_age").click();// Edad
		driver.findElementById("com.newbrick.usound:id/acb_dap_save").click();// Save
		driver.findElementById("com.newbrick.usound:id/actv_ffr_gender").click();// Gender
		driver.findElementById("com.newbrick.usound:id/acrb_dig_female").click();// Female
		driver.findElementById("com.newbrick.usound:id/acrb_dig_male").click();// Male
		driver.findElementById("com.newbrick.usound:id/acb_dig_save").click();// Save
		edad = driver.findElementById("com.newbrick.usound:id/actv_ffr_age").getText();
		genero = driver.findElementById("com.newbrick.usound:id/actv_ffr_gender").getText();
		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/acb_ffr_start").click();// Start
									
		//ACCESO A LAS PESTAÑAS
		Thread.sleep(2000);	
		int pest3X = x-x/4;
		int pest3Y = y/5;
		TouchAction t = new TouchAction(driver);			
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3		

		Thread.sleep(1000);
		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// Detalle_Izquierdo
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// Ingresar_audiometria

		l125 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l125").getText();
		l250 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l250").getText();
		l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		l8000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l8000").getText();
		r125 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r125").getText();
		r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		r250 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r250").getText();
		r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();
		r8000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r8000").getText();

	}
	  
	@Test(priority = 2)
	public void instalar() throws Exception {	
				
		Thread.sleep(6000);
		driver.pressKeyCode(AndroidKeyCode.HOME);
		Thread.sleep(6000);			
		//Buscar en Store
		driver.findElementByXPath("//android.widget.TextView[@text='Play Store']").click();
		Thread.sleep(4000);
		driver.findElementById("com.android.vending:id/search_box_idle_text").click();
		Thread.sleep(4000);
		driver.findElementById("com.android.vending:id/search_box_text_input").sendKeys("uSound");
		Thread.sleep(4000);
		TouchAction t = new TouchAction(driver);
		Dimension dimensions = driver.manage().window().getSize();
		double x = dimensions.getWidth();
		double y = dimensions.getHeight();
		int lupaX = (int) (x-x * 0.1);
		int lupaY = (int) (y-y * 0.07);
		t.tap(lupaX, lupaY).perform().release();
		Thread.sleep(4000);
		t = new TouchAction(driver);
		driver.findElementByXPath("//android.widget.TextView[@text='uSound (Asistente auditivo)']").click();
		Thread.sleep(4000);		
		t = new TouchAction(driver);
		driver.findElementByXPath("//android.widget.Button[@text='ACTUALIZAR']").click();
		Thread.sleep(4000);
		driver.findElementById("com.android.vending:id/continue_button").click();
		Thread.sleep(30000);//Instalando...
	}	  
	  
	@Test(priority = 3)
	public void verifica_cuenta() throws Exception {

		// ACCESO A MENU
		Thread.sleep(4000);
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int menuX = x - x / 20;
		int menuY = y / 10;
		TouchAction t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();
		t = new TouchAction(driver);
		t.tap(menuX, menuY).perform().release();

		driver.findElementByXPath("(//android.widget.RelativeLayout)[1]").click();// micuenta
		Thread.sleep(4000);
		String txt_nombre = driver.findElementById("com.newbrick.usound:id/actv_fpr_name").getText();
		String txt_email = driver.findElementById("com.newbrick.usound:id/actv_fpr_email").getText();
		String txt_edad = driver.findElementById("com.newbrick.usound:id/actv_fpr_age").getText();
		String txt_genero = driver.findElementById("com.newbrick.usound:id/actv_fpr_gender").getText();
		String txt_licence = driver.findElementById("com.newbrick.usound:id/actv_fpr_expiration_date").getText();
		Assert.assertEquals(txt_nombre, nombre);
		Assert.assertEquals(txt_email, email);
		Assert.assertEquals(txt_edad, edad);
		Assert.assertEquals(txt_genero, genero);
		Assert.assertEquals(txt_licence, licence);
	}

	@Test(priority = 4)
	public void verifica_audim() throws Exception {
		Thread.sleep(2000);		
		
		//ACCESO A LAS PESTAÑAS
		Thread.sleep(2000);	
		Dimension dimensions = driver.manage().window().getSize();			
		int x = dimensions.getWidth();			
		int y = dimensions.getHeight();
		int pest3X = x-x/4;
		int pest3Y = y/5;
		TouchAction t = new TouchAction(driver);			
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3					
		Thread.sleep(3000);
		
		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// Detalle Izquierdo 
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// Ingresar audiometria

		Thread.sleep(500);
		String txt_l125 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l125").getText();
		String txt_l250 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l250").getText();
		String txt_l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		String txt_l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		String txt_l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		String txt_l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		String txt_l8000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l8000").getText();
		String txt_r125 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r125").getText();
		String txt_r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		String txt_r250 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r250").getText();
		String txt_r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		String txt_r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		String txt_r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();
		String txt_r8000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r8000").getText();

		Assert.assertEquals(txt_l125, l125);
		Assert.assertEquals(txt_l250, l250);
		Assert.assertEquals(txt_l500, l500);
		Assert.assertEquals(txt_l1000, l1000);
		Assert.assertEquals(txt_l2000, l2000);
		Assert.assertEquals(txt_l4000, l4000);
		Assert.assertEquals(txt_l8000, l8000);
		Assert.assertEquals(txt_r125, r125);
		Assert.assertEquals(txt_r250, r250);
		Assert.assertEquals(txt_r500, r500);
		Assert.assertEquals(txt_r1000, r1000);
		Assert.assertEquals(txt_r2000, r2000);
		Assert.assertEquals(txt_r4000, r4000);
		Assert.assertEquals(txt_r8000, r8000);
	}

	
	
	  @AfterMethod
	  public void aftertearDown() throws Exception {
		  driver.quit();
	  }
	  
}
