package com.usound.testng;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;

//Device					Device Name				SO	
//Moto G1 (rooteado)		TA9300A6QN				5.0.2				
//Samsung S6				06157df6090a6417		7.0
//Moto G4 (rafa)			ZY22446KQ7				6.0.1

public class TestAuditivo {

	private AndroidDriver<AndroidElement> driver;	
	String apk_name = "uSound_v2.5.1-freqLabel.apk";
	String device_name = "ZY22446KQ7";
	String platform_version = "7.0";	
	String nombre = "Rafael Rodriguez";
	String email = "rafael@usound.co";
	String edad = "18 años";
	String genero = "Masculino";
	String licence = "Tu licencia está activa";
	String l125;
	String l250;
	String l500;
	String l1000;
	String l2000;
	String l4000;
	String l8000;
	String r125;
	String r250;
	String r500;
	String r1000;
	String r2000;
	String r4000;
	String r8000;

	@BeforeMethod
	  public void beforeMethod() throws MalformedURLException {

			File f = new File("src");
			File fs = new File(f, apk_name);
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, platform_version);
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, device_name);
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
			cap.setCapability("noReset", "true");
			driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);	
	  }
	
	@Test(priority = 1)
	public void test_normal() throws Exception {	
		Thread.sleep(2000);
		String l500;
		String l1000;
		String l2000;
		String l4000;
		String r500;
		String r1000;
		String r2000;
		String r4000;
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3		
		int testX = x / 2;
		int testY = y / 2;
		int wait = 0;
		driver.findElementById("com.newbrick.usound:id/acb_fbr_new_test").click();// NuevoTest
		driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
		// VERIFICAR OIDO
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		int i = 0;		
		while (i <= 14) {
			Thread.sleep(wait);
			t = new TouchAction(driver);	
			t.tap(testX, testY).perform().release();									
			i++;
		}
		String txt_Lnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_leresults").getText();
		String txt_Rnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_reresults").getText();
		String txt_Lpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_lepercent").getText();
		String txt_Rpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_repercent").getText();
		
		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// DetalleI
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();
		
		int val_l500 = Integer.valueOf(l500);
		int val_l1000 = Integer.valueOf(l1000);
		int val_l2000 = Integer.valueOf(l2000);
		int val_l4000 = Integer.valueOf(l4000);
		int val_r500 = Integer.valueOf(r500);
		int val_r1000 = Integer.valueOf(r1000);
		int val_r2000 = Integer.valueOf(r2000);
		int val_r4000 = Integer.valueOf(r4000);
		
		double lpercent = ((val_l500+val_l1000+val_l2000+val_l4000)/4-10)*100/80;
		int trunc_lpercent = (int) Math.floor(lpercent);
		String lporc = String.valueOf(trunc_lpercent)+"%";
		
		double rpercent = ((val_r500+val_r1000+val_r2000+val_r4000)/4-10)*100/80;
		int trunc_rpercent = (int) Math.floor(rpercent);
		String rporc = String.valueOf(trunc_rpercent)+"%";
		
		Assert.assertEquals(txt_Lnormal, "Sin pérdida\nde audición");
		Assert.assertEquals(txt_Rnormal, "Sin pérdida\nde audición");
		Assert.assertEquals(txt_Lpercent, lporc);
		Assert.assertEquals(txt_Rpercent, rporc);			
		}	
	
	public void test_leve() throws Exception {	
		Thread.sleep(2000);
		String l500;
		String l1000;
		String l2000;
		String l4000;
		String r500;
		String r1000;
		String r2000;
		String r4000;
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3		
		int testX = x / 2;
		int testY = y / 2;
		int wait = 7000;
		driver.findElementById("com.newbrick.usound:id/acb_fbr_new_test").click();// NuevoTest
		driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
		// VERIFICAR OIDO
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		int i = 0;		
		while (i <= 14) {
			Thread.sleep(wait);
			t = new TouchAction(driver);	
			t.tap(testX, testY).perform().release();									
			i++;
		}
		String txt_Lnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_leresults").getText();
		String txt_Rnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_reresults").getText();
		String txt_Lpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_lepercent").getText();
		String txt_Rpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_repercent").getText();
//		System.out.println("Left: "+ txt_Lnormal);
//		System.out.println("Right: "+ txt_Rnormal);
//		System.out.println("Left percent: "+ txt_Lpercent);
//		System.out.println("Right percent: "+ txt_Rpercent);
		
		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// DetalleI
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();
		
		int val_l500 = Integer.valueOf(l500);
		int val_l1000 = Integer.valueOf(l1000);
		int val_l2000 = Integer.valueOf(l2000);
		int val_l4000 = Integer.valueOf(l4000);
		int val_r500 = Integer.valueOf(r500);
		int val_r1000 = Integer.valueOf(r1000);
		int val_r2000 = Integer.valueOf(r2000);
		int val_r4000 = Integer.valueOf(r4000);
		
		double lpercent = ((val_l500+val_l1000+val_l2000+val_l4000)/4-10)*100/80;
		int trunc_lpercent = (int) Math.floor(lpercent);
		String lporc = String.valueOf(trunc_lpercent)+"%";
		
		double rpercent = ((val_r500+val_r1000+val_r2000+val_r4000)/4-10)*100/80;
		int trunc_rpercent = (int) Math.floor(rpercent);
		String rporc = String.valueOf(trunc_rpercent)+"%";
		
//		System.out.println("porcentaje truncado left: "+ lporc);
//		System.out.println("porcentaje truncado right: "+ rporc);
		
		Assert.assertEquals(txt_Lnormal, "Pérdida leve\nde audición");
		Assert.assertEquals(txt_Rnormal, "Pérdida leve\nde audición");
		Assert.assertEquals(txt_Lpercent, lporc);
		Assert.assertEquals(txt_Rpercent, rporc);			
		}

	@Test(priority = 2)
	public void test_leves() throws Exception {	
		Thread.sleep(2000);
		String l500;
		String l1000;
		String l2000;
		String l4000;
		String r500;
		String r1000;
		String r2000;
		String r4000;
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3		
		int testX = x / 2;
		int testY = y / 2;
		int wait = 7000;
		driver.findElementById("com.newbrick.usound:id/acb_fbr_new_test").click();// NuevoTest
		driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
		// VERIFICAR OIDO
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		int i = 0;		
		while (i <= 14) {
			Thread.sleep(wait);
			t = new TouchAction(driver);	
			t.tap(testX, testY).perform().release();									
			i++;
		}
		String txt_Lnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_leresults").getText();
		String txt_Rnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_reresults").getText();
		String txt_Lpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_lepercent").getText();
		String txt_Rpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_repercent").getText();
//		System.out.println("Left: "+ txt_Lnormal);
//		System.out.println("Right: "+ txt_Rnormal);
//		System.out.println("Left percent: "+ txt_Lpercent);
//		System.out.println("Right percent: "+ txt_Rpercent);
		
		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// DetalleI
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();
		
		int val_l500 = Integer.valueOf(l500);
		int val_l1000 = Integer.valueOf(l1000);
		int val_l2000 = Integer.valueOf(l2000);
		int val_l4000 = Integer.valueOf(l4000);
		int val_r500 = Integer.valueOf(r500);
		int val_r1000 = Integer.valueOf(r1000);
		int val_r2000 = Integer.valueOf(r2000);
		int val_r4000 = Integer.valueOf(r4000);
		
		double lpercent = ((val_l500+val_l1000+val_l2000+val_l4000)/4-10)*100/80;
		int trunc_lpercent = (int) Math.floor(lpercent);
		String lporc = String.valueOf(trunc_lpercent)+"%";
		
		double rpercent = ((val_r500+val_r1000+val_r2000+val_r4000)/4-10)*100/80;
		int trunc_rpercent = (int) Math.floor(rpercent);
		String rporc = String.valueOf(trunc_rpercent)+"%";
		
//		System.out.println("porcentaje truncado left: "+ lporc);
//		System.out.println("porcentaje truncado right: "+ rporc);
		
		Assert.assertEquals(txt_Lnormal, "Pérdida leve\nde audición");
		Assert.assertEquals(txt_Rnormal, "Pérdida leve\nde audición");
		Assert.assertEquals(txt_Lpercent, lporc);
		Assert.assertEquals(txt_Rpercent, rporc);			
		}		
	
	@Test(priority = 2)
	public void test_moderada() throws Exception {	
		Thread.sleep(2000);
		String l500;
		String l1000;
		String l2000;
		String l4000;
		String r500;
		String r1000;
		String r2000;
		String r4000;
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3		
		int testX = x / 2;
		int testY = y / 2;
		int wait = 12000;
		driver.findElementById("com.newbrick.usound:id/acb_fbr_new_test").click();// NuevoTest
		driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
		// VERIFICAR OIDO
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		int i = 0;		
		while (i <= 14) {
			Thread.sleep(wait);
			t = new TouchAction(driver);	
			t.tap(testX, testY).perform().release();									
			i++;
		}
		String txt_Lnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_leresults").getText();
		String txt_Rnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_reresults").getText();
		String txt_Lpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_lepercent").getText();
		String txt_Rpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_repercent").getText();
//		System.out.println("Left: "+ txt_Lnormal);
//		System.out.println("Right: "+ txt_Rnormal);
//		System.out.println("Left percent: "+ txt_Lpercent);
//		System.out.println("Right percent: "+ txt_Rpercent);
		
		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// DetalleI
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();
		
		int val_l500 = Integer.valueOf(l500);
		int val_l1000 = Integer.valueOf(l1000);
		int val_l2000 = Integer.valueOf(l2000);
		int val_l4000 = Integer.valueOf(l4000);
		int val_r500 = Integer.valueOf(r500);
		int val_r1000 = Integer.valueOf(r1000);
		int val_r2000 = Integer.valueOf(r2000);
		int val_r4000 = Integer.valueOf(r4000);
		
		double lpercent = ((val_l500+val_l1000+val_l2000+val_l4000)/4-10)*100/80;
		int trunc_lpercent = (int) Math.floor(lpercent);
		String lporc = String.valueOf(trunc_lpercent)+"%";
		
		double rpercent = ((val_r500+val_r1000+val_r2000+val_r4000)/4-10)*100/80;
		int trunc_rpercent = (int) Math.floor(rpercent);
		String rporc = String.valueOf(trunc_rpercent)+"%";
		
//		System.out.println("porcentaje truncado left: "+ lporc);
//		System.out.println("porcentaje truncado right: "+ rporc);
		
		Assert.assertEquals(txt_Lnormal, "Pérdida moderada\nde audición");
		Assert.assertEquals(txt_Rnormal, "Pérdida moderada\nde audición");
		Assert.assertEquals(txt_Lpercent, lporc);
		Assert.assertEquals(txt_Rpercent, rporc);			
		}		

	@Test(priority = 3)
	public void test_severa() throws Exception {	
		Thread.sleep(2000);
		String l500;
		String l1000;
		String l2000;
		String l4000;
		String r500;
		String r1000;
		String r2000;
		String r4000;
		Dimension dimensions = driver.manage().window().getSize();
		int x = dimensions.getWidth();
		int y = dimensions.getHeight();
		int pest3X = x - x / 4;
		int pest3Y = y / 5;
		TouchAction t = new TouchAction(driver);
		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3		
		int testX = x / 2;
		int testY = y / 2;
		int wait = 18000;
		driver.findElementById("com.newbrick.usound:id/acb_fbr_new_test").click();// NuevoTest
		driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
		// VERIFICAR OIDO
		driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
		driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
		int i = 0;		
		while (i <= 14) {
			Thread.sleep(wait);
			t = new TouchAction(driver);	
			t.tap(testX, testY).perform().release();									
			i++;
		}
		String txt_Lnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_leresults").getText();
		String txt_Rnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_reresults").getText();
		String txt_Lpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_lepercent").getText();
		String txt_Rpercent = driver.findElementById("com.newbrick.usound:id/actv_fbr_repercent").getText();
//		System.out.println("Left: "+ txt_Lnormal);
//		System.out.println("Right: "+ txt_Rnormal);
//		System.out.println("Left percent: "+ txt_Lpercent);
//		System.out.println("Right percent: "+ txt_Rpercent);
		
		driver.findElementById("com.newbrick.usound:id/acb_fbr_ledetails").click();// DetalleI
		driver.findElementById("com.newbrick.usound:id/acb_fer_input_audiometry").click();// IngresarAudiometria
		l500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l500").getText();
		l1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l1000").getText();
		l2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l2000").getText();
		l4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_l4000").getText();
		r500 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r500").getText();
		r1000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r1000").getText();
		r2000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r2000").getText();
		r4000 = driver.findElementById("com.newbrick.usound:id/acet_bsa_r4000").getText();
		
		int val_l500 = Integer.valueOf(l500);
		int val_l1000 = Integer.valueOf(l1000);
		int val_l2000 = Integer.valueOf(l2000);
		int val_l4000 = Integer.valueOf(l4000);
		int val_r500 = Integer.valueOf(r500);
		int val_r1000 = Integer.valueOf(r1000);
		int val_r2000 = Integer.valueOf(r2000);
		int val_r4000 = Integer.valueOf(r4000);
		
		double lpercent = ((val_l500+val_l1000+val_l2000+val_l4000)/4-10)*100/80;
		int trunc_lpercent = (int) Math.floor(lpercent);
		String lporc = String.valueOf(trunc_lpercent)+"%";
		
		double rpercent = ((val_r500+val_r1000+val_r2000+val_r4000)/4-10)*100/80;
		int trunc_rpercent = (int) Math.floor(rpercent);
		String rporc = String.valueOf(trunc_rpercent)+"%";
		
//		System.out.println("porcentaje truncado left: "+ lporc);
//		System.out.println("porcentaje truncado right: "+ rporc);
		
		Assert.assertEquals(txt_Lnormal, "Pérdida severa\nde audición");
		Assert.assertEquals(txt_Rnormal, "Pérdida severa\nde audición");
		Assert.assertEquals(txt_Lpercent, lporc);
		Assert.assertEquals(txt_Rpercent, rporc);			
		}		
	
	
//@Test(priority = 1) //BARRIDO DEL TEST
//	public void test_auditivo() throws Exception {	
//		Thread.sleep(2000);
//		Dimension dimensions = driver.manage().window().getSize();
//		int x = dimensions.getWidth();
//		int y = dimensions.getHeight();
//		int pest3X = x - x / 4;
//		int pest3Y = y / 5;
//		TouchAction t = new TouchAction(driver);
//		t.tap(pest3X, pest3Y).perform().release(); // pestaña 3		
//		int testX = x / 2;
//		int testY = y / 2;
//		int wait = 11000;
//		while (wait <= 20000) {	
//			driver.findElementById("com.newbrick.usound:id/acb_fbr_new_test").click();// NuevoTest
//			driver.findElementById("com.newbrick.usound:id/rl_fad_rl1").click(); // BtnComenzar
//			// VERIFICAR OIDO
//			driver.findElementById("com.newbrick.usound:id/acb_fts_right_channel_preview").click(); // Derecho
//			driver.findElementById("com.newbrick.usound:id/acb_fts_start").click(); // Next
//			int i = 0;		
//			while (i <= 14) {
//				Thread.sleep(wait);
//				t = new TouchAction(driver);	
//				t.tap(testX, testY).perform().release();									
//				i++;
//			}
//			String txt_Lnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_leresults").getText();
//			String txt_Rnormal = driver.findElementById("com.newbrick.usound:id/actv_fbr_reresults").getText();
//			System.out.println("Espera: "+ wait);
//			System.out.println("Left: "+ txt_Lnormal);
//			System.out.println("Right: "+ txt_Rnormal);
//			wait = wait + 1000;			
//		}		
//	}
		
	  @AfterMethod
	  public void aftertearDown() throws Exception {
		  driver.quit();
	  }
	  
}